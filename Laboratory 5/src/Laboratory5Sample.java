import java.util.Scanner;


public class Laboratory5Sample {
	public static void main(String[] args){
		
		/*
		//System.out.println("Enter Pet Owner's Name: ");
		//Scanner sc=new Scanner(System.in);
		
		//declaration
		//PetOwner po;
		PetOwner po=new PetOwner(18);
		//po.name=sc.nextLine();//instantiation
		
		//System.out.println("Hello "+po.name +"!");
		 
		 */
		PetOwner po=new PetOwner(18);
		System.out.println("Printing outside object for "+po.getName());
		System.out.println("Instantiating Pet ....");
		po.buyPet();
		//po.namePet();
		
		System.out.println("Changing name to Arian ....");
		po.setName("Arian");
		
		System.out.println("Hello "+po.getName());
	}

}
