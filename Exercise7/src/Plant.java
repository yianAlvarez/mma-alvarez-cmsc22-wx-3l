
public class Plant {

	protected String name;
	protected int life;
	protected int damage;
	protected int suncost;
	protected int rechargerate;
	
	public Plant(){
		
		
	}
	public Plant(String name, int life, int damage, int suncost, int rechargerate){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		
	}
	
	public void move(){
		
		System.out.println(this.name+ " is moving");
	}
	
	public void recharge(){
		
		System.out.println(this.name+ " is recharging in "+rechargerate+" sunperminute");
	}

	public void upgrade(){
	
	System.out.println(this.name+ " is upgrading");
	}
	public void defend(){
		
		System.out.println(this.name+ " is defending");
	}
	
	
}
