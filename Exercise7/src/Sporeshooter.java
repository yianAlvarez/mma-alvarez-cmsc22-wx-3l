
public class Sporeshooter extends Plant implements Sporeshooteri{
	
	private int range;
	private int speed;
	
	public Sporeshooter(String name, int life, int damage, int suncost, int rechargerate){
		super(name, life, damage, suncost, rechargerate);
	}
	
	public Sporeshooter(String name, int life, int damage, int suncost, int rechargerate, int range, int speed){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.range=range;
		this.speed=speed;
		
	}
	
	public void shootspores(){
		
		System.out.println(this.name+" is shooting spores with a range of " +this.range+ " and a speed of "+this.speed+".");
	}

}
