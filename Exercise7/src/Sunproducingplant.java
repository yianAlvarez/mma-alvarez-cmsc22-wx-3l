
public class Sunproducingplant extends Plant implements SunProduceI{
	int numberofproducedsun;
	public Sunproducingplant(String name, int life, int damage, int suncost, int rechargerate){
		super(name, life, damage, suncost, rechargerate);
	}
	public Sunproducingplant(String name, int life, int damage, int suncost, int rechargerate, int numberofproducesun){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.numberofproducedsun=numberofproducesun;
	}
	
	public void producesun(int num) {
		System.out.println("Produces"+num);
		
	}

}
