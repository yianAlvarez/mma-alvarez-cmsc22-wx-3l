
public class Peashooter extends Plant implements PeashooterI{
	
	private int numberofpeas;
	private int speed;
	
	public Peashooter(String name, int life, int damage, int suncost, int rechargerate){
		super(name, life, damage, suncost, rechargerate);
	}
	public Peashooter(String name, int life, int damage, int suncost, int rechargerate,int numberofpeas,  int speed){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.numberofpeas=numberofpeas;
		this.speed=speed;
	}
	public Peashooter(){
	
	}
	public void shoots(){
		
		System.out.println(this.name+ " is shooting "+numberofpeas+ "pieces "+speed+" per minute.");
	}

}
