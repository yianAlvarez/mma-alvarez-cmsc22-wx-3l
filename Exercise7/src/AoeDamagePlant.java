
public class AoeDamagePlant extends Plant implements AoeDamageI{
	private int areaOfEffect;
	
	public AoeDamagePlant(String name, int life, int damage, int suncost, int rechargerate){
		
		super(name, life, damage, suncost, rechargerate);
	}
	public AoeDamagePlant(String name, int life, int damage, int suncost, int rechargerate, int areaOfEffect){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.areaOfEffect=areaOfEffect;
	}
	
	public void destroy() {
		System.out.println(this.name+"destroys an area of "+areaOfEffect+".");
		
	}
	

}
