
public class Lobbedshotplant extends Plant implements LobbedshotI {
		int range;
	
	public Lobbedshotplant(String name, int life, int damage, int suncost, int rechargerate){
		super(name, life, damage, suncost, rechargerate);
	}
	public Lobbedshotplant(String name, int life, int damage, int suncost, int rechargerate,int range){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.range=range;

	}
	
	
	public void throwenemy() {
		System.out.println(this.name+" is throwing a pumpkin to the zombie");
		
	}

}
