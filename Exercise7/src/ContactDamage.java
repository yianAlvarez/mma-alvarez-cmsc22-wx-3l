
public class ContactDamage extends Plant implements ContactDamageI {
	
	
	int speedofeffect;
	
	
	public ContactDamage(String name, int life, int damage, int suncost, int rechargerate){
		super(name, life, damage, suncost, rechargerate);
	}
	public ContactDamage(String name, int life, int damage, int suncost, int rechargerate, int speedofeffect){
		this.name=name;
		this.life=life;
		this.damage=damage;
		this.suncost=suncost;
		this.rechargerate=rechargerate;
		this.speedofeffect=speedofeffect;
	}
	
	public void detonates() {
		System.out.println(this.name+"managed to detonate with a damage of "+this.damage+"and a speed of effect "+this.speedofeffect);
		
	}

	public void eat() {
		System.out.println("The plant is eating.");
		
	}

}
