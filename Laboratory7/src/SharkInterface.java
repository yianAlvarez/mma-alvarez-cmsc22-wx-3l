
public interface SharkInterface {
	
	public void swim();
	public void eatHuman();
	public void reproduce(int offspring);

}
