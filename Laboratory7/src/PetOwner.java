
public class PetOwner {
	private String name;
	private int age;
	//private Pet[] pets;
	private Shark[] sharks;
	public PetOwner(){
		
		
	}
	public PetOwner(String name, int age, int petCount){
		this.name=name;
		this.age=age;
		//this.pets=new Pet[petCount];
		this.sharks=new Shark[petCount];
		//this.initPets();
		this.initSharks();
		
	}

	
public String getName(){
		
		return this.name;
	}
	
	public void setName(String name){
		
		this.name=name;
	}
	
	public int getAge(){
		
		return this.age;
	}
	
	public void setAge(int age){
		
		this.age=age;
	}
	/*public void initPets(){
		
		
		for(int i=0; i<pets.length;i++){
			
			pets[i]=new Pet("Pet "+(i+1), i*10);
			
		}
	}*/
	/*
	public void printDetails(){
		System.out.println("Name: "+this.name);
		System.out.println("Age: "+this.age);
		for(int i=0;i<pets.length;i++){
			System.out.println(pets[i].getName()+" "+pets[i].getAge());
		}
		
	}*/
	
	public void initSharks(){
		for(int i=0; i<sharks.length; i++){
			sharks[i]=new Shark("Shark"+(i+1), i*10, 2+i, 100*i);
		}
	}
	public void printDetailss(){
		System.out.println("Name: "+this.name);
		System.out.println("Age: "+this.age);
		for(int i=0;i<sharks.length;i++){
			System.out.println(sharks[i].getName()+" "+sharks[i].getAge()+sharks[i].getFinCount()+sharks[i].getTeeth());
		}
		
	}
	
	public void feedSharks(){
		
		for(int i=0; i<sharks.length;i++){
			sharks[i].swim();
			sharks[i].eatHuman();
			sharks[i].reproduce(i*5);
		}
	}
}
