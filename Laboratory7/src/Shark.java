
public class Shark extends Pet implements SharkInterface{

		private int finCount;
		private int teeth;
		
		public Shark(){
			
		}
		
		public Shark(String name, int age){
			
			super(name, age);
		}
	
		public Shark(String name, int age, int finCount, int teeth){
			super(name, age);
			this.finCount=finCount;
			this.teeth=teeth;
		}
		
		public void eatHuman(){
			System.out.println(this.name+ " swallowed Arian.");
		}
		public int getFinCount(){
			
			return this.finCount;
		}
		public int getTeeth(){
			
			return this.teeth;
		}


		public void swim() {
			
			System.out.println(this.name+" is swimming.");
		}


		public void reproduce(int offspring) {
	
			System.out.println(this.name+" gave birth to " +offspring + " new sharklings.");
		}
	
}

