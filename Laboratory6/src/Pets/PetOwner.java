package Pets;
import java.util.Scanner;

import Pets.Pet;


public class PetOwner {
	private Pet[] pets;
	private String name;
	
	
	public PetOwner(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Name:");
		this.name=sc.nextLine();
	}
	
	public void initPets(){
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter pet count");
		int count=sc.nextInt();
		//initialize a n number of pets
		this.pets=new Pet[count];
		
		for(int i=0; i<count; i++){
			
			pets[i]=new Pet("Pet "+i);
			
		}
		
		
	}
	//getter name
	public String getName(){
		
		return this.name;
	}
	
	//setter name of owner
	public void setName(String name){
		this.name=name;
		
	}
	
	public Pet[] getPets(){
		
		return this.pets;
	}

	public void setPets(Pet[] pets){
		this.pets=pets;
	}
}
