package Pets;

public class Pet {
	private String name;
	
	public Pet(String name){
		
		this.name=name;
		
	}
	
	//getter function
	public String getName(){
		
		return this.name;
	}
	//setter function
	public void setName(String name){
		
		this.name=name;
	}

}
